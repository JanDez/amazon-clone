import React from 'react'
import './Header.css'

function header() {
    return (
        <div className='header'>
            <img 
                className="header__logo"
                src='http://pngimg.com/uploads/amazon/amazon_PNG11.png'
            />
            
            <div
                className="header__search">
                <input 
                    className="header__searchInput" type="text"
                />
                {/*Logo*/}
            </div>
            <div
                className="header__nav">
                <div className='header__option'>
                    <span classNanme='header__optionLineOne'>
                        Hello user
                    </span>
                    <span classNanme='header__optionLineTwo'>
                        Sign In 
                    </span>
                </div>
                <div className='header__option'>
                    <span classNanme='header__optionLineOne'>
                        Returns
                    </span>
                    <span classNanme='header__optionLineTwo'>
                        & Orders
                    </span>
                </div>
                <div className='header__option'>
                    <span classNanme='header__optionLineOne'>
                        Your
                    </span>
                    <span classNanme='header__optionLineTwo'>
                        Prime 
                    </span>
                </div>
                <div className='header__option'>

                </div>
            </div>
        </div>
    );
}

export default header
